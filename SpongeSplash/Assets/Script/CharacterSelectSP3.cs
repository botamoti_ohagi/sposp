using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSelectSP3 : MonoBehaviour
{
    public static int P3_SelectSponge;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Sponge1")
        {
            P3_SelectSponge = 1;
        }

        if (other.gameObject.tag == "Sponge2")
        {
            P3_SelectSponge = 2;
        }

        if (other.gameObject.tag == "Sponge3")
        {
            P3_SelectSponge = 3;
        }

        if (other.gameObject.tag == "Sponge4")
        {
            P3_SelectSponge = 4;
        }
    }
}
