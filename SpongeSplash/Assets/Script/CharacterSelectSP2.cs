using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSelectSP2 : MonoBehaviour
{
    public static int P2_SelectSponge;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag=="Sponge1")
        {
            P2_SelectSponge = 1;
        }

        if(other.gameObject.tag=="Sponge2")
        {
            P2_SelectSponge = 2;
        }

        if (other.gameObject.tag == "Sponge3")
        {
            P2_SelectSponge = 3;
        }

        if (other.gameObject.tag == "Sponge4")
        {
            P2_SelectSponge = 4;
        }
    }
}
