using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character3 : MonoBehaviour
{
    public GameObject SpongePreview1;
    public GameObject SpongePreview2;
    public GameObject SpongePreview3;
    public GameObject SpongePreview4;

    public GameObject SpawnPoint;
    Vector3 SpawnPos;

    // Start is called before the first frame update
    void Start()
    {
        SpawnPos = SpawnPoint.transform.position;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SelectCharacter()
    {

 
        GameObject Player = Instantiate(SpongePreview3);
        Player.transform.position = SpawnPoint.transform.position;
    }
}
