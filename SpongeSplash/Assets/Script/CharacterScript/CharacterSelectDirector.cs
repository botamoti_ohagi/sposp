using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterSelectDirector : MonoBehaviour
{
    public static int P1_SelectSponge;
    public static int P2_SelectSponge;
    public static int P3_SelectSponge;
    public static int P4_SelectSponge;

    public bool P1_DecisionBool=false;
    public bool P2_DecisionBool=false;
    public bool P3_DecisionBool=false;
    public bool P4_DecisionBool=false;

    int LoadStage;
         
    // Update is called once per frame
    void Update()
    {
        if(P1_DecisionBool==true&& P2_DecisionBool == true/* && P3_DecisionBool == true*/) /*&& P4_DecisionBool == true)*/
        {
            LoadStage = 1;/*Random.Range(1, 3);*/
        }

        if(LoadStage==1)
        {
            SceneManager.LoadScene("SinkScene");
        }

        if(LoadStage==2)
        {
            SceneManager.LoadScene("BathScene");
        }

        if(LoadStage==3)
        {
            SceneManager.LoadScene("CarScene");
        }

        Debug.Log(P1_DecisionBool);
        Debug.Log(P1_SelectSponge);
    }

    public void P1_SetBool()
    {
        P1_DecisionBool = true;
    }

    public void P2_SetBool()
    {
        P2_DecisionBool = true;
    }

    public void P3_SetBool()
    {
        P3_DecisionBool = true;
    }

    public void P4_SetBool()
    {
        P4_DecisionBool = true;
    }
}