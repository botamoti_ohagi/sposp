using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P3_StartSplashParticle : MonoBehaviour
{
    public ParticleSystem SplashParticle;
    // Start is called before the first frame update
    void Start()
    {
        SplashParticle = GetComponent<ParticleSystem>();
    }

    public void StartPariticle()
    {
        SplashParticle.Play();
    }
}
