using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KasaAnimation : MonoBehaviour
{
    [SerializeField]
    private Animator animator;

    public void Kasa_Start()
    {
        animator.SetTrigger("GuardTrigger");
    }

    public void Kasa_Stay()
    {
        animator.SetBool("GuardBool", true);
    }

    public void Kasa_End()
    {
        animator.SetBool("GuardBool", false);
    }
}
