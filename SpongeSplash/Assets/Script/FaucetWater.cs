using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaucetWater : MonoBehaviour
{
    ParticleSystem Water;
    float ParticleStartTime;
    // Start is called before the first frame update
 
    void Update()
    {
        ParticleStartTime += Time.deltaTime;

        Debug.Log(ParticleStartTime);

        //もしParticleStartTimeが10以上なら
        if (ParticleStartTime >= 10)
        {
            GetComponent<ParticleSystem>().Play();
        }

        //もしParticleStartTimeが30以下なら
        if (ParticleStartTime >= 30)
        {
            GetComponent<ParticleSystem>();
            ParticleStartTime = 0;
        }
    }

    public void WaterStart()
    {
        Water.Play();
    }

    public void WaterStop()
    {
        Water.Stop();
    }
}
