using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultScript : MonoBehaviour
{
    public GameObject []SpongeModel;//リザルトで表示するスポンジのモデル
    public GameObject Sponge;
    public GameObject P1_WinnerUI;
    public GameObject P2_WinnerUI;
    public GameObject P3_WinnerUI;
    public GameObject P4_WinnerUI;

    // Start is called before the first frame update
    void Start()
    {
        GameObject SpawnPoint = GameObject.Find("SpawnPoint");
        Vector3 SpawnPosition = SpawnPoint.transform.position;

        //プレイヤー1が生き残った場合
        if (GameDirector.SvPLNum==1)
        {
            GameObject Spawn = Instantiate(SpongeModel[GameDirector.P1_SelectCharacter]);//プレイヤー1が選択したキャラクターを表示
            Spawn.transform.position = SpawnPosition;
            P1_WinnerUI.SetActive(true);
        }

        //プレイヤー1,2,3,4のうち誰かが選んだキャラクターが2だったら
        if (GameDirector.SvPLNum==2)
        {
            GameObject Spawn = Instantiate(SpongeModel[GameDirector.P2_SelectCharacter]);
            Spawn.transform.position = SpawnPosition;
            P2_WinnerUI.SetActive(true);
        }

        //プレイヤー1,2,3,4のうち誰かが選んだキャラクターが3だったら
        if (GameDirector.SvPLNum==3)
        {
            GameObject Spawn = Instantiate(SpongeModel[GameDirector.P3_SelectCharacter]);
            Spawn.transform.position = SpawnPosition;
            P3_WinnerUI.SetActive(true);
        }

        //プレイヤー1,2,3,4のうち誰かが選んだキャラクターが4だったら
        if (GameDirector.SvPLNum==4)
        {
            GameObject Spawn = Instantiate(SpongeModel[GameDirector.P4_SelectCharacter]);
            Spawn.transform.position = SpawnPosition;
            P4_WinnerUI.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(GameDirector.SvPLNum);
        Debug.Log(GameDirector.P1_SelectCharacter);
    }
}
