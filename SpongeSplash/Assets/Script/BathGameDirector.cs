using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BathGameDirector : MonoBehaviour
{
    float ParticleStartTime;

    public ParticleSystem Water;
    public int PLnum = 0;
    public GameObject WaterParticle;

    public GameObject FloorWaterParticle;
    float FLParticleStartTime;
    // Update is called once per frame

    public GameObject SpongeModel1;
    public GameObject SpongeModel2;
    public GameObject SpongeModel3;
    public GameObject SpongeModel4;

    public GameObject P1SpawnPoint;
    public GameObject P2SpawnPoint;
    public GameObject P3SpawnPoint;
    public GameObject P4SpawnPoint;

    Vector3 P1Spawn;
    Vector3 P2Spawn;
    Vector3 P3Spawn;
    Vector3 P4Spawn;

    private void Start()
    {
        int P1_SelectCharacter = CharacterSelectDirector.P1_SelectSponge;
        int P2_SelectCharacter = CharacterSelectDirector.P2_SelectSponge;
        int P3_SelectCharacter = CharacterSelectDirector.P3_SelectSponge;
        int P4_SelectCharacter = CharacterSelectDirector.P4_SelectSponge;

        P1Spawn = P1SpawnPoint.transform.position;　//プレイヤーを生成する位置
        P2Spawn = P2SpawnPoint.transform.position;
        P3Spawn = P3SpawnPoint.transform.position;
        P4Spawn = P4SpawnPoint.transform.position;

        //プレイヤー１が選択したスポンジを表示
        if (P1_SelectCharacter == 1)
        {
            GameObject obj = Instantiate(SpongeModel1);
            obj.transform.position = new Vector3(-9.5f, 64.1f, -24);
        }

        if (P1_SelectCharacter == 2)
        {
            GameObject obj = Instantiate(SpongeModel1);
            obj.transform.position = new Vector3(-9.5f, 64.1f, -24);
        }

        if (P1_SelectCharacter == 3)
        {
            GameObject obj = Instantiate(SpongeModel3);
            obj.transform.position = new Vector3(-9.5f, 64.1f, -24);
        }

        if (P1_SelectCharacter == 4)
        {
            GameObject obj = Instantiate(SpongeModel4);
            obj.transform.position = new Vector3(-9.5f, 64.1f, -24);
        }

        //プレイヤー2が選択したスポンジを表示
        if (P2_SelectCharacter == 1)
        {
            GameObject obj = Instantiate(SpongeModel1);
            obj.transform.position = new Vector3(19.6f, 64.1f, -24);
        }

        if (P2_SelectCharacter == 2)
        {
            GameObject obj = Instantiate(SpongeModel2);
            obj.transform.position = new Vector3(19.6f, 64.1f, -24);
        }

        if (P2_SelectCharacter == 3)
        {
            GameObject obj = Instantiate(SpongeModel3);
            obj.transform.position = new Vector3(19.6f, 64.1f, -24);
        }

        if (P2_SelectCharacter == 4)
        {
            GameObject obj = Instantiate(SpongeModel4);
            obj.transform.position = new Vector3(19.6f, 64.1f, -24);
        }

        //プレイヤー3が選択したスポンジを表示
        if (P3_SelectCharacter == 1)
        {
            GameObject obj = Instantiate(SpongeModel1);
            obj.transform.position = new Vector3(-9.5f, 64.1f, 12);
        }

        if (P3_SelectCharacter == 2)
        {
            GameObject obj = Instantiate(SpongeModel2);
            obj.transform.position = new Vector3(-9.5f, 64.1f, 12);
        }

        if (P3_SelectCharacter == 3)
        {
            GameObject obj = Instantiate(SpongeModel3);
            obj.transform.position = new Vector3(-9.5f, 64.1f, 12);
        }

        if (P3_SelectCharacter == 4)
        {
            GameObject obj = Instantiate(SpongeModel4);
            obj.transform.position = new Vector3(-9.5f, 64.1f, 12);
        }

        //プレイヤー4が選択したスポンジを表示
        if (P4_SelectCharacter == 1)
        {
            GameObject obj = Instantiate(SpongeModel4);
            obj.transform.position = new Vector3(22.5f, 64.1f, 9.26f);
        }

        if (P4_SelectCharacter == 2)
        {
            GameObject obj = Instantiate(SpongeModel4);
            obj.transform.position = new Vector3(22.5f, 64.1f, 9.26f);
        }

        if (P4_SelectCharacter == 3)
        {
            GameObject obj = Instantiate(SpongeModel4);
            obj.transform.position = new Vector3(22.5f, 64.1f, 9.26f);
        }

        if (P4_SelectCharacter == 4)
        {
            GameObject obj = Instantiate(SpongeModel4);
            obj.transform.position = new Vector3(22.5f, 64.1f, 9.26f);
        }

    }

    private void Update()
    {
        ParticleStartTime += Time.deltaTime;

        FLParticleStartTime += Time.deltaTime;

        if (ParticleStartTime >= 10) 
        {
            WaterParticle.SetActive(true);//蛇口から出てくる水を生成
        }

        if (FLParticleStartTime >= 16)
        {
            FloorWaterParticle.SetActive(true);//床を流れる水を生成
        }

        if (ParticleStartTime >= 30)
        {
            WaterParticle.SetActive(false);//蛇口から出てくる水を停止
            ParticleStartTime = 0;
        }

        Debug.Log(ParticleStartTime);

        if (FLParticleStartTime >= 33)
        {
            FloorWaterParticle.SetActive(false);//床を流れる水を停止
        }


        Debug.Log(PLnum);
    }
    public void PlayerCount()
    {
        PLnum++;
    }

    public void LoadResult()
    {
        if (PLnum == 3)
        {
            SceneManager.LoadScene("ResultScene");
        }
    }
}
