using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CHSELPlayerController : MonoBehaviour
{
    public float speed; //プレイヤーの動くスピード

    private Vector3 Player_pos; //プレイヤーのポジション

    GameObject Player;
    int count;//水パーティクルに当たった回数
    public float shotSpeed;//ボールのスピード

    public GameObject Camera;

    public int PlayerID;

    public Material[] material;


    GameObject SpawnPoint;//ボールの生成位置
    public GameObject SpongeModel;
    public GameObject Sponge;

    Vector3 prevPos;    //直前の位置
    Vector3 nowPos;
  

    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = 60;
        Player_pos = GetComponent<Transform>().position; //最初の時点でのプレイヤーのポジションを取得
        prevPos = Player_pos;
        SpawnPoint = transform.GetChild(0).gameObject; //ボールの発射位置を取得
    }

    // Update is called once per frame
    void Update()
    {
        nowPos = transform.position;
       
        //移動処理
        {
            float x = 0, z = 0;
            if (PlayerID == 1)
            {
                x = Input.GetAxis("Horizontal");  //横方向の入力値（-1.0〜1.0）
                z = Input.GetAxis("Vertical");    // 縦方向の入力値（-1.0〜1.0）
            }
            if (PlayerID == 2)
            {
                x = Input.GetAxis("Horizontal2");  //横方向の入力値（-1.0〜1.0）
                z = Input.GetAxis("Vertical2");    // 縦方向の入力値（-1.0〜1.0）
            }
            if (PlayerID == 3)
            {
                x = Input.GetAxis("Horizontal3");  //横方向の入力値（-1.0〜1.0）
                z = Input.GetAxis("Vertical3");    // 縦方向の入力値（-1.0〜1.0）
            }
            if (PlayerID == 4)
            {
                x = Input.GetAxis("Horizontal4");  //横方向の入力値（-1.0〜1.0）
                z = Input.GetAxis("Vertical4");    // 縦方向の入力値（-1.0〜1.0）
            }
            //移動
            this.GetComponent<CharacterController>().SimpleMove(new Vector3(x, 0, z) * Time.deltaTime * speed);
        }

        //向き
        {
            //1フレームでどのくらいうごいたか（移動量）
            Vector3 move = new Vector3(transform.position.x - prevPos.x, 0, transform.position.z - prevPos.z);


            //動いてたら　（動いてないなら向き変えなくていい）
            if (move.magnitude > 0)
            {
                //現在の位置＋移動量＝次のフレームで行く予定の位置　を向くように回転させる
                transform.LookAt(transform.position + move);

                //現在の位置を更新
                prevPos = transform.position;
                Sponge.GetComponent<AnimationScript>().WalkAnimation();
            }
            else
            {
                Sponge.GetComponent<AnimationScript>().IdleAnimation();

            }
        }


        //キャラクターの選択を決定
        if (Input.GetKeyDown(KeyCode.Joystick1Button0) && PlayerID == 1 && CharacterSelectDirector.P1_SelectSponge > 0)
        {
            Camera.GetComponent<CharacterSelectDirector>().P1_SetBool();
        }

        if (Input.GetKeyDown(KeyCode.Joystick2Button0) && PlayerID == 2 && CharacterSelectDirector.P2_SelectSponge > 0)
        {
            Camera.GetComponent<CharacterSelectDirector>().P2_SetBool();
        }

        //if (Input.GetKeyDown(KeyCode.Joystick3Button0) && PlayerID == 3 && CharacterSelectDirector.P3_SelectSponge > 0)
        //{
        //    Camera.GetComponent<CharacterSelectDirector>().P3_SetBool();
        //}

        //if(Input.GetKeyDown(KeyCode.Joystick4Button0) && PlayerID == 4&&CharacterSelectDirector.P4_SelectSponge>0)
        //{
        //    Camera.GetComponent<CharacterSelectDirector>().P4_SetBool();
        //}
    }

    private void OnControllerColliderHit(ControllerColliderHit other)
    {
        //選択したキャラクターの番号を代入
        if (other.gameObject.tag == "Sponge1")
        {
            if (PlayerID == 1)
            {
                CharacterSelectDirector.P1_SelectSponge = 1;
            }

            if (PlayerID == 2)
            {
                CharacterSelectDirector.P2_SelectSponge = 1;
            }

            if (PlayerID == 3)
            {
                CharacterSelectDirector.P3_SelectSponge = 1;
            }

            if (PlayerID == 4)
            {
                CharacterSelectDirector.P4_SelectSponge = 1;
            }
        }

        if (other.gameObject.tag == "Sponge2")
        {
            if (PlayerID == 1)
            {
                CharacterSelectDirector.P1_SelectSponge = 2;
            }

            if (PlayerID == 2)
            {
                CharacterSelectDirector.P2_SelectSponge = 2;
            }

            if (PlayerID == 3)
            {
                CharacterSelectDirector.P3_SelectSponge = 2;
            }

            if (PlayerID == 4)
            {
                CharacterSelectDirector.P4_SelectSponge = 2;
            }
        }

        if (other.gameObject.tag == "Sponge3")
        {
            if (PlayerID == 1)
            {
                CharacterSelectDirector.P1_SelectSponge = 3;
            }

            if (PlayerID == 2)
            {
                CharacterSelectDirector.P2_SelectSponge = 3;
            }

            if (PlayerID == 3)
            {
                CharacterSelectDirector.P3_SelectSponge = 3;
            }

            if (PlayerID == 4)
            {
                CharacterSelectDirector.P4_SelectSponge = 3;
            }
        }

        if (other.gameObject.tag == "Sponge4")
        {
            if (PlayerID == 1)
            {
                CharacterSelectDirector.P1_SelectSponge = 4;
            }

            if (PlayerID == 2)
            {
                CharacterSelectDirector.P2_SelectSponge = 4;
            }

            if (PlayerID == 3)
            {
                CharacterSelectDirector.P3_SelectSponge = 4;
            }

            if (PlayerID == 4)
            {
                CharacterSelectDirector.P4_SelectSponge = 4;
            }
        }
    }
}
       



