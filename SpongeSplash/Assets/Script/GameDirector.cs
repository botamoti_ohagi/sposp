using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameDirector : MonoBehaviour
{
    float LoadTime;//シーンロードを開始するまでの時間
    public int PLnum = 0;
    public static GameObject SurvivePlayer;
    public static int SvPLNum;

    public GameObject [] SpongeModel;

    public GameObject P1SpawnPoint;
    public GameObject P2SpawnPoint;
    public GameObject P3SpawnPoint;
    public GameObject P4SpawnPoint;

    Vector3 P1Spawn;
    Vector3 P2Spawn;
    Vector3 P3Spawn;
    Vector3 P4Spawn;

    public static int P1_SelectCharacter; //プレイヤー1が選んだキャラクター
    public static int P2_SelectCharacter; //プレイヤー2が選んだキャラクター
    public static int P3_SelectCharacter; //プレイヤー3が選んだキャラクター
    public static int P4_SelectCharacter; //プレイヤー4が選んだキャラクター

    private void Start()
    {
        //選択したスポンジのキャラクターの番号
        P1_SelectCharacter = CharacterSelectDirector.P1_SelectSponge;　
        P2_SelectCharacter = CharacterSelectDirector.P2_SelectSponge;
        P3_SelectCharacter = CharacterSelectDirector.P3_SelectSponge;
        P4_SelectCharacter = CharacterSelectDirector.P4_SelectSponge;

        //プレイヤーを生成する位置
        P1Spawn = P1SpawnPoint.transform.position;　
        P2Spawn = P2SpawnPoint.transform.position;
        P3Spawn = P3SpawnPoint.transform.position;
        P4Spawn = P4SpawnPoint.transform.position;

        //プレイヤー１が選択したスポンジを表示
        if(P1_SelectCharacter == 1)
        {
            GameObject obj = Instantiate(SpongeModel[0]);
            obj.transform.position = new Vector3(-9.5f, 64.1f, -24);
        }

        if(P1_SelectCharacter == 2)
        {
            GameObject obj = Instantiate(SpongeModel[1]);
            obj.transform.position = new Vector3(-9.5f, 64.1f, -24);
        }

        if(P1_SelectCharacter == 3)
        {
            GameObject obj = Instantiate(SpongeModel[2]);
            obj.transform.position = new Vector3(-9.5f, 64.1f, -24);
        }

        if(P1_SelectCharacter == 4)
        {
            GameObject obj = Instantiate(SpongeModel[3]);
            obj.transform.position = new Vector3(-9.5f, 64.1f, -24);
        }

        //プレイヤー2が選択したスポンジを表示
        if (P2_SelectCharacter == 1)
        {
            GameObject obj = Instantiate(SpongeModel[4]);
            obj.transform.position = new Vector3(19.6f, 64.1f, -24);
        }

        if (P2_SelectCharacter == 2)
        {
            GameObject obj = Instantiate(SpongeModel[5]);
            obj.transform.position = new Vector3(19.6f, 64.1f, -24);
        }

        if (P2_SelectCharacter == 3)
        {
            GameObject obj = Instantiate(SpongeModel[6]);
            obj.transform.position = new Vector3(19.6f, 64.1f, -24);
        }

        if (P2_SelectCharacter == 4)
        {
            GameObject obj = Instantiate(SpongeModel[7]);
            obj.transform.position = new Vector3(19.6f, 64.1f, -24);
        }

        //プレイヤー3が選択したスポンジを表示
        if (P3_SelectCharacter == 1)
        {
            GameObject obj = Instantiate(SpongeModel[8]);
            obj.transform.position = new Vector3(-9.5f, 64.1f, 12);
        }

        if (P3_SelectCharacter == 2)
        {
            GameObject obj = Instantiate(SpongeModel[9]);
            obj.transform.position = new Vector3(-9.5f, 64.1f, 12);
        }

        if (P3_SelectCharacter == 3)
        {
            GameObject obj = Instantiate(SpongeModel[10]);
            obj.transform.position = new Vector3(-9.5f, 64.1f, 12);
        }

        if (P3_SelectCharacter == 4)
        {
            GameObject obj = Instantiate(SpongeModel[11]);
            obj.transform.position = new Vector3(-9.5f, 64.1f, 12);
        }

        //プレイヤー4が選択したスポンジを表示
        if (P4_SelectCharacter == 1)
        {
            GameObject obj = Instantiate(SpongeModel[12]);
            obj.transform.position = new Vector3(22.5f, 64.1f, 9.26f);
        }

        if (P4_SelectCharacter == 2)
        {
            GameObject obj = Instantiate(SpongeModel[13]);
            obj.transform.position = new Vector3(22.5f, 64.1f, 9.26f);
        }

        if (P4_SelectCharacter == 3)
        {
            GameObject obj = Instantiate(SpongeModel[14]);
            obj.transform.position = new Vector3(22.5f, 64.1f, 9.26f);
        }

        if (P4_SelectCharacter == 4)
        {
            GameObject obj = Instantiate(SpongeModel[15]);
            obj.transform.position = new Vector3(22.5f, 64.1f, 9.26f);
        }

    }

    private void Update()
    {
        LoadTime += Time.deltaTime;
        if(LoadTime>=3.5)
        {
            LoadTime = 0;
        }

        if (PLnum==3)//もしHPが0になったプレイヤーが3人になったら
        {
            if(LoadTime>=3.0)//もしLoadTimeが3秒になったら
            {
                SceneManager.LoadScene("ResultScene");//ResultSceneをロードする
            }
        }
      
    }
    //HPがゼロになったキャラクターをカウント
    public void PlayerCount()
    {
        PLnum++;
    }

    //リザルトシーン読み込み
    public void LoadResult()
    {
        if (PLnum == 3)
        {
            SceneManager.LoadScene("ResultScene");
        }
    }
}