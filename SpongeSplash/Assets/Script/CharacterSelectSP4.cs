using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSelectSP4 : MonoBehaviour
{
    public static int P4_SelectSponge;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Sponge1")
        {
            P4_SelectSponge = 1;
        }

        if (other.gameObject.tag == "Sponge2")
        {
            P4_SelectSponge = 2;
        }

        if (other.gameObject.tag == "Sponge3")
        {
            P4_SelectSponge = 3;
        }

        if (other.gameObject.tag == "Sponge4")
        {
            P4_SelectSponge = 4;
        }
    }
}
