using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterParticleController : MonoBehaviour
{
    float ParticleStartTime;
    public ParticleSystem Water;
    public GameObject WaterParticle;

    // Update is called once per frame
    private void Start()
    {
        
    }

    void Update()
    {
        ParticleStartTime += Time.deltaTime;

        if(ParticleStartTime>=10)
        {
            WaterParticle.SetActive(true);
        }
    }
}
