using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationScript : MonoBehaviour
{
    [SerializeField]
    private Animator animator;

    public void WalkAnimation()
    {
        animator.SetBool("Walkbool", true);
        animator.SetBool("Idlebool", false);
    }

    public void IdleAnimation()
    {
        animator.SetBool("Walkbool", false);
        animator.SetBool("Idlebool", true);
    }

    public void ThrowAnimation()
    {
        animator.SetTrigger("ThrowTrigger");
    }

    public void SqeezeAnimation()
    {
        Debug.Log("kita");
        animator.SetTrigger("SqeezeTrigger");
    }

    public void FalldownAnimation()
    {
        animator.SetTrigger("FalldownTrigger");
    }

    public void DamageAnimation()
    {
        animator.SetTrigger("DamageTrigger");
    }

    public void StayGuardAnimation_Start()
    {
        animator.SetTrigger("Sponge_Guard_StartTrigger");
    }

    public void StayGuardAnimation_Stay()
    {
        animator.SetBool("Sponge_Guard_StayBool", true);
    }

    public void StayGuardAnimation_End()
    {
        animator.SetBool("Sponge_Guard_StayBool", false);
        animator.SetTrigger("Sponge_Guard_EndTrigger");

    }

    public void WalkGuardAnimation_Start()
    {
        animator.SetTrigger("WalkGuardStartTrigger");
    }

    public void WalkGuardAnimation_Stay()
    {
        animator.SetBool("WalkGuardStayBool", true);
    }

    public void WalkGuardAnimation_End()
    {
        animator.SetTrigger("WalkGuardEndTrigger");
    }

    public void WalkGuardtoStayGuard()
    {
        animator.SetBool("WalkGuardtoStayGuard", true);
        animator.SetBool("StayGuardtoWalkGuard", false);
    }

    public void StayGuardtoWalkGuard()
    {
        animator.SetBool("StayGuardtoWalkGuard", true);
        animator.SetBool("WalkGuardtoStayGuard", false);
    }

    public void GuardStart_Kasa()
    {
        animator.SetBool("KasaGuardStart", true);
    }

    public void GuardStay_Kasa()
    {
        animator.SetBool("KasaGuardStay", true);
    }

    public void GuardEnd_Kasa()
    {
        animator.SetBool("KasaGuardEnd", false);
    }

   
    
    
}
