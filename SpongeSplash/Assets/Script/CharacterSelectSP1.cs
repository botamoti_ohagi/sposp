using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSelectSP1 : MonoBehaviour
{
    public static int P1_SelectSponge;
    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Sponge1")
        {
            P1_SelectSponge = 1;
        }

        if(other.gameObject.tag=="Sponge2")
        {
            P1_SelectSponge = 2;
        }

        if(other.gameObject.tag=="Sponge3")
        {
            P1_SelectSponge = 3;
        }

        if(other.gameObject.tag=="Sponge4")
        {
            P1_SelectSponge = 4;
        }

        Debug.Log(P1_SelectSponge);
    }
}
