﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerController : MonoBehaviour
{
    public float speed; //プレイヤーの動くスピード

    private Vector3 Player_pos; //プレイヤーのポジション

    public int HP=4;
    GameObject Player;
    int count;//水パーティクルに当たった回数

    public GameObject WaterBallPrefab;//水風船のプレハブ
    public float shotSpeed;//ボールのスピード

    public GameObject[] SpongePrefab;//スポンジのプレハブ
    
    public int PlayerID;

    public Material[] material;

    float mutekiTime = 0;
    float Squeeztime;//次に絞れるまでの時間
    float ShotInterval=1;
    float ThrowDelay = 0;

    GameObject SpawnPoint;//ボールの生成位置
    public GameObject SpongeModel;
    public GameObject Sponge;
    public GameObject Kasa;
    GameObject Director;

    Vector3 prevPos;    //直前の位置
    Vector3 nowPos;
    int NowHP;//今のHP

    bool GurdBool;//ガードしているかどうか
    bool HitBool=true;

    //最後まで生き残ったキャラクター
    public static int P1_SvPlCharacter;
    public static int P2_SvPlCharacter;
    public static int P3_SvPlCharacter;
    public static int P4_SvPlCharacter;

    //public bool FallBool = false; //プレイヤーが落下しているか

    void Start()
    {
        Application.targetFrameRate = 60;
        Player_pos = GetComponent<Transform>().position; //最初の時点でのプレイヤーのポジションを取得
        prevPos = Player_pos;
        SpawnPoint = transform.GetChild(0).gameObject; //ボールの発射位置を取得
    }

    // Update is called once per frame
    void Update()
    {
        nowPos = transform.position;
        NowHP = HP;
        Director = GameObject.Find("GameDirector");

        

        if (HP >1)
        {
            //移動処理
            {
                float x = 0, z = 0;
                if (PlayerID == 1)
                {
                    x = Input.GetAxis("Horizontal");  //横方向の入力値（-1.0～1.0）
                    z = Input.GetAxis("Vertical");    // 縦方向の入力値（-1.0～1.0）
                }
                if (PlayerID == 2)
                {
                    x = Input.GetAxis("Horizontal2");  //横方向の入力値（-1.0～1.0）
                    z = Input.GetAxis("Vertical2");    // 縦方向の入力値（-1.0～1.0）
                }
                if (PlayerID == 3)
                {
                    x = Input.GetAxis("Horizontal3");  //横方向の入力値（-1.0～1.0）
                    z = Input.GetAxis("Vertical3");    // 縦方向の入力値（-1.0～1.0）
                }
                if (PlayerID == 4)
                {
                    x = Input.GetAxis("Horizontal4");  //横方向の入力値（-1.0～1.0）
                    z = Input.GetAxis("Vertical4");    // 縦方向の入力値（-1.0～1.0）
                }
                //移動
                this.GetComponent<CharacterController>().SimpleMove(new Vector3(x, 0, z) * Time.deltaTime * speed);
            }

            //向き
            {
                //1フレームでどのくらいうごいたか（移動量）
                Vector3 move = new Vector3(transform.position.x - prevPos.x, 0, transform.position.z - prevPos.z);


                //動いてたら　（動いてないなら向き変えなくていい）
                if (move.magnitude > 0)
                {
                    //現在の位置＋移動量＝次のフレームで行く予定の位置　を向くように回転させる
                    transform.LookAt(transform.position + move);

                    //現在の位置を更新
                    prevPos = transform.position;

                    //歩きアニメーションを再生
                    Sponge.GetComponent<AnimationScript>().WalkAnimation();


                    //歩き状態でのガードアニメーションスタート
                    if(PlayerID == 1 && Input.GetKey(KeyCode.Joystick1Button2) || PlayerID == 2 && Input.GetKey(KeyCode.Joystick2Button2) || PlayerID == 3 && Input.GetKey(KeyCode.Joystick3Button2) || PlayerID == 4 && Input.GetKey(KeyCode.Joystick4Button2))
                    {
                        Sponge.GetComponent<AnimationScript>().WalkGuardAnimation_Start();
                        Sponge.GetComponent<AnimationScript>().WalkGuardAnimation_Stay();
                        Sponge.GetComponent<AnimationScript>().StayGuardtoWalkGuard();
                    }

                    //歩き状態でのガードアニメーション終了
                    if (PlayerID == 1 && Input.GetKeyUp(KeyCode.Joystick1Button2) || PlayerID == 2 && Input.GetKeyUp(KeyCode.Joystick2Button2) || PlayerID == 3 && Input.GetKeyUp(KeyCode.Joystick3Button2) || PlayerID == 4 && Input.GetKeyUp(KeyCode.Joystick4Button2))
                    {
                        Sponge.GetComponent<AnimationScript>().WalkGuardAnimation_End();
                    }

                }
                else
                {
                    //待機アニメーションを再生
                    Sponge.GetComponent<AnimationScript>().IdleAnimation();

                    if (PlayerID == 1 && Input.GetKey(KeyCode.Joystick1Button2) || PlayerID == 2 && Input.GetKey(KeyCode.Joystick2Button2) || PlayerID == 3 && Input.GetKey(KeyCode.Joystick3Button2) || PlayerID == 4 && Input.GetKey(KeyCode.Joystick4Button2))
                    {
                        Sponge.GetComponent<AnimationScript>().WalkGuardtoStayGuard();
                    }
                }

            }

            //投げる
            ShotInterval += Time.deltaTime;
          
            if ((PlayerID == 1 && Input.GetKeyDown(KeyCode.Joystick1Button5)) && GurdBool == false || (PlayerID == 2 && Input.GetKeyDown(KeyCode.Joystick2Button5)) && GurdBool == false || (PlayerID == 3 && Input.GetKeyDown(KeyCode.Joystick3Button5)) && GurdBool == false || (PlayerID == 4 && Input.GetKeyDown(KeyCode.Joystick4Button5) && GurdBool == false))
            {
                ThrowDelay += Time.deltaTime;
                ThrowDelay =0;
                
                if (ShotInterval >= 1)
                {
                    Sponge.GetComponent<AnimationScript>().ThrowAnimation();
                    //if(ThrowDelay>=0.7)
                    //{
                        GameObject WaterBall = Instantiate(WaterBallPrefab);//ボールの生成
                        WaterBall.GetComponent<Rigidbody>().AddForce(transform.forward * shotSpeed);//右に10の力で押す 
                        ShotInterval = 0;//ShotIntervalリセット
                        ThrowDelay = 0;
                        WaterBall.transform.position = SpawnPoint.transform.position;//ボールの位置をスポーンポイントの位置にする

                        WaterBall.GetComponent<BallController>().PlayerID = PlayerID;

                    //}
                }
            }
            
            //絞る
            if ((PlayerID == 1&& Input.GetKeyDown(KeyCode.Joystick1Button1) && HP < 4 || PlayerID == 2&& Input.GetKeyDown(KeyCode.Joystick2Button1) && HP < 4 && prevPos == nowPos || PlayerID == 3 && Input.GetKeyDown(KeyCode.Joystick3Button1) && HP < 4|| PlayerID == 4 && Input.GetKeyDown(KeyCode.Joystick4Button1) && HP < 4) && prevPos == nowPos&&Squeeztime>=2.0f)
            {
                Sponge.GetComponent<AnimationScript>().SqeezeAnimation();
                Squeeztime += Time.deltaTime;
                HP++;
                Squeeztime = 0;
            }
            Squeeztime += Time.deltaTime;
            if(NowHP!=HP)
            {
                Squeeztime = Time.deltaTime;
            }

            //ガード
            if (PlayerID == 1 && Input.GetKey(KeyCode.Joystick1Button2) || PlayerID == 2 && Input.GetKey(KeyCode.Joystick2Button2)|| PlayerID == 3 && Input.GetKey(KeyCode.Joystick3Button2)|| PlayerID == 4 && Input.GetKey(KeyCode.Joystick4Button2))
            {
                Sponge.GetComponent<AnimationScript>().StayGuardAnimation_Start();//ガードアニメーション
                Sponge.GetComponent<AnimationScript>().StayGuardAnimation_Stay();

                Kasa.GetComponent<KasaAnimation>().Kasa_Start();
                Kasa.GetComponent<KasaAnimation>().Kasa_Stay();

                GameObject KasaModel = transform.Find("Umbrella").gameObject;
                KasaModel.SetActive(true);

                GameObject obj = transform.Find("Kasa").gameObject;
                obj.SetActive(true);

              

                GurdBool = true;
            }

            if (PlayerID == 1 && Input.GetKeyUp(KeyCode.Joystick1Button2) || PlayerID == 2 && Input.GetKeyUp(KeyCode.Joystick2Button2) || PlayerID == 3 && Input.GetKeyUp(KeyCode.Joystick3Button2) || PlayerID == 4 && Input.GetKeyUp(KeyCode.Joystick4Button2))
            {
                GameObject KasaModel = transform.Find("Umbrella").gameObject;
                KasaModel.SetActive(false);
                Sponge.GetComponent<AnimationScript>().StayGuardAnimation_End();//止まった状態でのガードアニメーション終了
                Sponge.GetComponent<AnimationScript>().WalkGuardAnimation_End();//歩き状態でのガードアニメーション終了

                Kasa.GetComponent<KasaAnimation>().Kasa_End();


                GurdBool = false;
            }
        }
        else//HPがゼロだったら
        {
            Sponge.GetComponent<AnimationScript>().FalldownAnimation();
            //GetComponent<BoxCollider>().enabled = false;//コライダーをオフにする
            GetComponent<CharacterController>().enabled = false;//コライダーをオフにする
            transform.GetChild(0).gameObject.GetComponent<BoxCollider>().enabled = false;
        }
        

        ///////ステージギミック判定/////////////////////
        if (count >= 10)//水パーティクルに40回以上当たったとき
        {
            HP--;//HPをマイナス
            if(HP<=0)
            {
                HP = 1;
            }
            SpongeModel.GetComponent<Renderer>().sharedMaterial = material[HP - 1];//HP-1のマテリアルを適用
        }


        mutekiTime += Time.deltaTime;
        //Debug.Log(mutekiTime);
        /////////////////////////////////////PLnum後で3に変更////////////////////////////////////////////////////////
        if (Director.GetComponent<GameDirector>().PLnum == 1 && HP >= 2)
        {
            Debug.Log(PlayerID);
            GameDirector.SvPLNum = PlayerID;
            //if (GameDirector.SvPLNum == 1)
            //{
            //    P1_SvPlCharacter = Director.GetComponent<GameDirector>().P1_SelectCharacter;
            //}

            //if (GameDirector.SvPLNum == 2)
            //{
            //    P2_SvPlCharacter = Director.GetComponent<GameDirector>().P2_SelectCharacter;
            //}

            //if (GameDirector.SvPLNum == 3)
            //{
            //    P3_SvPlCharacter = Director.GetComponent<GameDirector>().P3_SelectCharacter;
            //}

            //if (GameDirector.SvPLNum == 4)
            //{
            //    P4_SvPlCharacter = Director.GetComponent<GameDirector>().P4_SelectCharacter;
            //}
        }
    }


    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "WaterBall" && other.gameObject.GetComponent<BallController>().PlayerID != PlayerID)//ぶつかったもののタグがWaterBallかつ他のプレイヤーのものだったら
        {
            GameObject P1_Splash = GameObject.Find("P1_Splash");//Splashパーティクルを検索
            GameObject P2_Splash = GameObject.Find("P2_Splash");//Splashパーティクルを検索
            GameObject P3_Splash = GameObject.Find("P3_Splash");//Splashパーティクルを検索
            GameObject P4_Splash = GameObject.Find("P4_Splash");//Splashパーティクルを検索
            gameObject.GetComponent<BoxCollider>().enabled = false;//コライダーをオフにする
            mutekiTime = 0;

            if (PlayerID == 1)
            {
                P1_Splash.GetComponent<P1_StartSplashParticle>().StartPariticle();//プレイヤーIDが1のプレイヤーのSplashパーティクルを再生
            }

            if (PlayerID == 2)
            {
                P2_Splash.GetComponent<P2_StartSplashParticle>().StartPariticle();//プレイヤーIDが2のプレイヤーのSplashパーティクルを再生
            }

            if (PlayerID == 3)
            {
                P3_Splash.GetComponent<P3_StartSplashParticle>().StartPariticle();//プレイヤーIDが3のプレイヤーのSplashパーティクルを再生
            }

            if (PlayerID == 4)
            {
                P4_Splash.GetComponent<P4_StartSplashParticle>().StartPariticle();//プレイヤーIDが4のプレイヤーのSplashパーティクルを再生
            }

            GetComponent<AudioSource>().Play();
            HP--;//HPをマイナス
            SpongeModel.GetComponent<Renderer>().sharedMaterial = material[HP - 1];//HP-1のマテリアルを適用
            Sponge.GetComponent<AnimationScript>().DamageAnimation();



            if(HP<=1)//HPが1以下になったらGameDirectorのPlayerCountを呼ぶ
            {
                Director.GetComponent<GameDirector>().PlayerCount();
            }



        


            if (HP<0)
            {
                HP = 1;
            }

            //無敵時間
            if (mutekiTime >= 5)
            {
                gameObject.GetComponent<BoxCollider>().enabled = true;//コライダーをオフにする
            }

        }
    }

  
    void OnControllerColliderHit(ControllerColliderHit other) //キャラクターコントローラーをもったものと衝突した時
    {

        if (other.gameObject.tag=="Sponge"&&HP<4&&HitBool==true) //ぶつかった相手のタグがSpongeで自分のHPが4より少なかったらだったら
        {
            other.gameObject.GetComponent<PlayerController>().HP -= 1; //相手のHPを減らす  
        }
        //HitBool = false;


        if (other.gameObject.tag == "GOfloor")
        {
            HP = 0; 
        }


    

        //if (other.gameObject.tag == "WaterBall" && other.gameObject.GetComponent<BallController>().PlayerID != PlayerID)//ぶつかったもののタグがWaterBallかつ他のプレイヤーのものだったら
        //{

        //    GetComponent<AudioSource>().Play();
        //    HP--;//HPをマイナス
        //    if (HP < 0)
        //    {
        //        HP = 0;
        //    }


        //    //mutekiflag = true;

        //    //GetComponent<CharacterController>().enabled = false;
        //    //GetComponent<BoxCollider>().enabled = false;
        //}
    }

  
    void OnParticleCollision(GameObject other)
    {
        count++;
    }

    void OnControllerColliderExit(ControllerColliderHit other)
    {
        HitBool = false;
    }
}
