using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleCount : MonoBehaviour
{
    GameObject Player;

    int count;
    private void Start()
    {
        Player = GameObject.Find("Player");
    }

    private void Update()
    {
        if(count>=40)
        {
            Player.GetComponent<PlayerController>().HP--;
        }
        Debug.Log(count);
    }
    private void OnParticleCollision(GameObject other)
    {
        count++;
    }
}
